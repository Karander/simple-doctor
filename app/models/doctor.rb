class Doctor < ApplicationRecord
    has_many :appointments
    has_many :patients, through: :appointments
    
    def available?(date)
        unless date.sunday?
            if date.saturday?
                basic_times=["08:00", "11:00", "14:00", "17:00", "18:00"]
            else
                basic_times=["08:00", "11:00", "14:00", "17:00", "18:00", "19:30", "20:00"]
            end
                appointments=self.appointments.where(ap_date: date)
                appointments.each do |ap|
                    basic_times.delete(ap.ap_time)
                end
            !basic_times.empty?
        end
    end
    
end
