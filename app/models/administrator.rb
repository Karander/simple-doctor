class Administrator < ApplicationRecord
    validates :name, presence: true
    validates :date_birth, presence: true
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true,  length: {maximum:255}, format: {with: VALID_EMAIL_REGEX},
                uniqueness: {case_sensitive: false}
    validate :unique_email
    has_secure_password
    
    def forget
        return true
    end
    
    def blocked?
        return true if blocked==true
    end
    
    private
    
    def unique_email
        self.errors.add(:email, "Email already taken") if Patient.where(email: self.email).exists?
    end
    
end
