class Patient < ApplicationRecord
    has_many :appointments, dependent: :destroy
    has_many :doctors, through: :appointments
    attr_accessor :remember_token
    before_save {self.email.downcase!}
    validates :name, presence: true, length: {minimum:5, maximum:50}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true,  length: {maximum:255}, format: {with: VALID_EMAIL_REGEX},
                uniqueness: {case_sensitive: false}
    has_secure_password
    validates :password, presence: true, length: {minimum:6}, allow_nil: true
    
    def Patient.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
    
    def Patient.new_token
        SecureRandom.urlsafe_base64
    end
    
    def remember
        self.remember_token=Patient.new_token
        update_attribute(:remember_digest, Patient.digest(remember_token))
    end
    
    def authenticated?(remember_token)
        return false if remember_digest.nil?
        BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end
    
    def forget
        update_attribute(:remember_digest, nil)
    end
    
    def blocked?
        return true if blocked==true
    end
    
    def self.search(name, email, blocked, combine)
        blocked_status=""
        if blocked=="on"
            blocked_status="t"
        end
        if combine=="on"
            where("name LIKE ? AND email LIKE ? AND blocked IS ?", "%#{name}%", "%#{email}%", "#{blocked_status}")
        else
            #set unavailable values for empty fields to avoid returning all patients
            name="{%&" if name==""
            email="{%&" if email==""
            blocked_status="{%&" if blocked_status==""
            where("name LIKE ? OR email LIKE ? OR blocked IS ?", "%#{name}%", "%#{email}%", "#{blocked_status}")
        end
    end

end
