class Message < ApplicationRecord
    
    validates :subject, presence: true
    validates :content, presence: true
    validates :creator_id, presence: true
    validates :creator_type, presence: true
    
end
