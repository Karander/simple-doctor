class Content < ApplicationRecord
    has_attached_file :image, styles: { common: "194x259" }
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    validates :content, presence: true
    validates :type, presence: true, inclusion: { in: ["doctor", "administrator"] }
end
