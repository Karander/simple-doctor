class Appointment < ApplicationRecord
  belongs_to :doctor
  belongs_to :patient
  validates :ap_time, presence: true, inclusion: { in: ["08:00", "11:00", "14:00", "17:00", "18:00", "19:30", "20:00"] },
                uniqueness: { scope: [:doctor_id, :ap_date] }
  validates :ap_time, uniqueness: { scope: [:patient_id, :ap_date] }
  validates :ap_date, presence: :true
    validate :not_sunday
    validate :date_not_today_or_past
    validate :limited_workinghours_on_saturday
  validates :doctor_id, presence: :true
  validates :patient_id, presence: :true
  
  def past?
    return true if ap_date < Date.today
  end
  
  private
  
  def not_sunday
    unless ap_date.nil?
      if ap_date.sunday?
        errors.add(:ap_date, "No appointments available on sundays.")
      end
    end
  end
  
  def date_not_today_or_past
    unless ap_date.nil?
      if ap_date.today?
        errors.add(:ap_date, "Please call us for short term appointments.")
      elsif ap_date.past?
        errors.add(:ap_date, "The date of your appointment can not be in the past.")
      end
    end
  end
  
  def limited_workinghours_on_saturday
    unless ap_date.nil?
      if ap_date.saturday?
        a=["08:00", "11:00", "14:00", "17:00", "18:00"]
        unless a.include?(ap_time)
          errors.add(:ap_time, "Workinghours are limited on Saturdays")
        end
      end
    end
  end
  
  def self.search(name, time, date, date_end, doctor, combine)
    if combine=="on"
      result=joins(:patient, :doctor).where("patients.name LIKE ? AND 
                            appointments.ap_time LIKE ? AND 
                            doctors.name LIKE ?", 
                            "%#{name}%", "%#{time}%", "%#{doctor}")
    else
      #set unavailable values to avoid returning all appointments
      name="{%&" if name==""
      time="{%&" if time==""
      doctor="{%&" if doctor==""
      result=joins(:patient, :doctor).where("(patients.name LIKE ? OR 
                            appointments.ap_time LIKE ? OR 
                            doctors.name LIKE ?)", 
                            "%#{name}%", "%#{time}%", "%#{doctor}")
    end
    
    result=result.where("ap_date >= ?", "#{date}") unless date==nil
    result=result.where("ap_date <= ?", "#{date_end}") unless date_end==nil
    
    result
    
  end
  
end
