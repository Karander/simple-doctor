$(document).on "ready page:load", ->
    $("div.history-dropdown").on "click", "#history-link", ->
        $("#history").fadeToggle()
        return false
    $("section.settings-dropdown").on "click", "#settings-link", ->
        $("div.settings").fadeToggle()
        return false
    $("section.settings-dropdown").on "click", "#delete-link", ->
        $("#delete").fadeToggle()
        return false