class AppointmentsController < ApplicationController
  # _sh: sessions_helper
  
  before_action :redirect_blocked_users_sh
  before_action :logged_in_administrator_sh, only: [:index]
  before_action :logged_in_patient_sh
  before_action :authorized_user, only: [:destroy]
  
  def index
    @appointments=Appointment.paginate(page: params[:page])
    if params[:pt_name]
      #check if start date is complete or return nil
      unless (params[:ap_day]=="Day" or params[:ap_month]=="Month" or params[:ap_year]=="Year")
        date=params[:ap_year]+"-"+params[:ap_month]+"-"+params[:ap_day]
        #check if start date is valid
        begin
          date=date.to_date
        #inform if start date is not valid
        rescue ArgumentError
          flash[:danger]= date + " is an invalid date. Search continued without date submission."
          date=nil
        end
      else
        date=nil
      end
      #check end date as above
      unless (params[:ap_day_e]=="Day" or params[:ap_month_e]=="Month" or params[:ap_year_e]=="Year")
        date_end=params[:ap_year_e]+"-"+params[:ap_month_e]+"-"+params[:ap_day_e]
        begin
          date_end=date_end.to_date
        rescue ArgumentError
          flash[:danger]+= date_end + " is an invalid date. Search continued without date submission."
          date_end=nil
        end
        #unless nil check if end date > start date
        if(date!=nil and date_end!=nil)
          if date > date_end
            date=nil
            date_end=nil
            flash[:danger]= "The start date must be earlier than the end date. Search continued without date submission."
          end
        end
      else
        date_end=nil
      end
      @appointments = Appointment.search(
          params[:pt_name], 
          params[:time], 
          date,
          date_end,
          params[:appointment][:doctor_id],
          params[:combine]).paginate(page: params[:page])
    else
      @appointments = Appointment.paginate(page: params[:page])
    end
  end
  
  def new
    @appointment=Patient.first.appointments.build
    @times=appointable_times(params[:doctor_id], params[:ap_date])
  end
  
  def create
    unless current_user_admin_sh?
      @appointment=current_user_sh.appointments.build(appointment_params)
    else
      user=Patient.find_by(id: params[:appointment][:patient_id])
      @appointment=user.appointments.build(appointment_params)
    end
    if @appointment.save
      flash[:success]="Appointment created"
      if current_user_admin_sh?
        redirect_to @appointment.patient
      else
        redirect_to current_user_sh
      end
    else
      if @appointment.errors.any?
        error_text="Appointment not created. Following errors occured:
          "
        @appointment.errors.full_messages.each do |msg|
          error_text += " -"+msg+"
            "
        end
      end
      flash[:danger]= error_text
      redirect_to calendar_path
    end
  end
  
  def destroy
    appointment=Appointment.find_by(id: params[:id])
    @appointment_to_delete=params[:id]
    #used in destroy.js.erb
    user=appointment.patient
    unless (appointment.past? and !current_user_admin_sh?)
      appointment.destroy
      respond_to do |format|
        format.html { flash[:success] = "Appointment deleted"
                    redirect_to user }
        format.js { flash.now[:success]= "Appointment deleted"}
      end
    else
      respond_to do |format|
        format.html { flash[:danger] = "You can not delete an past appointment"
                    redirect_to user }
        format.js { flash.now[:danger]= "You can not delete an past appointment"}
      end
    end
      
  end
  
  private
  
  def authorized_user
    appointment=Appointment.find_by(id: params[:id])
    redirect_to root_path unless(current_user_sh?(appointment.patient) or current_user_admin_sh?)
  end
  
  def appointment_params
     params.require(:appointment).permit(:ap_date, :patient_id, :doctor_id, :ap_time)
  end
  
  def appointable_times(doc_id, date)
      basic_times=["08:00", "11:00", "14:00", "17:00", "18:00", "19:30", "20:00"]
      doctor=Doctor.find_by(id: doc_id)
      if date.to_date.saturday?
          basic_times=basic_times[0..4]
      end
      doctor.appointments.where(ap_date: date).each do |doc_app|
          basic_times.delete(doc_app.ap_time)
      end
      @appointable_times=basic_times
  end
  
end
