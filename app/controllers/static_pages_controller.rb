class StaticPagesController < ApplicationController
  def home
    @content=Content.find_by(title: "home")
  end

  def contact
  end

  def about
    @about=Content.find_by(title: "about")
    @content=Content.new
    @contents=Content.all
  end
  
end
