class AdministratorsController < ApplicationController
  # _sh: sessions_helper
  
  before_action :redirect_blocked_users_sh, except: [:show]
  before_action :logged_in_administrator_sh
  before_action :correct_administrator_or_supervisor, only: [:show]
  before_action :administrator_is_supervisor, only: [:new, :create, :destroy, :edit, :update]
  
  def new
    @administrator=Administrator.new
  end
  
  def create
    @administrator=Administrator.new(administrator_params)
    if @administrator.save
      flash[:success]="New administrator created: " + @administrator.name
      redirect_to @administrator
    else
      render "administrators/new"
    end
  end

  def show
    @administrator=Administrator.find_by(id: params[:id])
    @messages=Message.where(recipient_type: "Administrator", recipient_id: [0, params[:id].to_i])
  end
  
  def edit
      @administrator=Administrator.find(params[:id])
  end
    
  def update
      if ( user_id = params[:user_to_block] )
          @administrator=Administrator.find(user_id)
          if @administrator.supervisor==false
            if @administrator.blocked?
                @administrator.update_attribute(:blocked, false)
                flash[:success]="Profile unblocked"
                redirect_to @administrator
            else
                @administrator.update_attribute(:blocked, true)
                flash[:success]="Profile blocked"
                redirect_to @administrator
            end
          else
            flash[:danger]="Supervisors can not be blocked"
            redirect_to @administrator
          end
      else
          @administrator=Administrator.find(params[:id])
          if @administrator.update_attributes(administrator_params)
             flash[:success]="Profile updated"
             redirect_to @administrator
          else
              render 'edit'
          end
      end
  end

  def index
    @administrators=Administrator.all
  end
  
  def destroy
    if Administrator.find_by(id: params[:id]).supervisor==true
      flash[:danger] = "Supervisors can not be deleted."
      redirect_to current_user_sh
    else
      Administrator.find_by(id: params[:id]).destroy
      @id_to_delete=params[:id]
      #used in destroy.js.erb
      respond_to do |format|
        format.html do
          flash[:success]="Administrator destroyed"
          redirect_to current_user_sh
        end
        format.js do
          flash.now[:success]="Administrator deleted"
        end
      end
    end
  end
  
  private
  
  def administrator_params
    params.require(:administrator).permit(:name, :email, :phone, :adress, :date_birth, :password, :password_confirmation)
  end
  
  def correct_administrator_or_supervisor
    unless( current_user_sh?(Administrator.find_by(id: params[:id]) ) or current_user_sh.supervisor==true )
      flash[:danger]="You are not authorized to perform this action."
      redirect_to root_path
    end
  end
  
  def administrator_is_supervisor
    unless current_user_sh.supervisor==true
      flash[:danger]="You are not authorized to perform this action."
      redirect_to root_path
    end
  end
  
end
