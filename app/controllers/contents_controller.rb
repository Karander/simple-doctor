class ContentsController < ApplicationController
    
    def update
        @content=Content.find(params[:id])
        if @content.update_attributes(update_params)
            respond_to do |format|
                format.html do
                    flash[:success]="Content edted"
                    redirect_to current_user_sh
                end
                format.js do
                    flash.now[:success]="Content edited"
                end
            end
        end
    end
    
  def create
    @content=Content.new(content_params)
    if @content.save
      flash[:success]="New content created"
      redirect_to about_path
    else
      flash[:danger]="No new content created"
      redirect_to current_user_sh
    end
  end
  
  def destroy
    Content.find_by(id: params[:id]).destroy
    @id_to_delete=params[:id]
    #used in destroy.js.erb
    respond_to do |format|
        format.html do
            flash[:success]="Content destroyed"
            redirect_to current_user_sh
        end
        format.js do
            flash.now[:success]="Content deleted"
        end
    end
  end
    
    private
    
    def content_params
        params.require(:content).permit(:content, :image, :object)
    end
    
    def update_params
        params.require(:content).permit(:content, :image)
    end
end
