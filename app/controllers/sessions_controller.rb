class SessionsController < ApplicationController
  # _sh: sessions_helper
  
  def new
  end
  
  def update
    if edit_session_sh?
      info="Editor mode has been turned off"
    else
      info="Editor mode has been turned on"
    end
    switch_edit_sh
    respond_to do |format|
      format.html do
        flash[:success]=info
        redirect_to current_user_sh
      end
        format.js do
        flash.now[:success]=info
      end
    end
  end
  
  def create
    user=Patient.find_by(email: params[:session][:email].downcase)
    user=Administrator.find_by(email: params[:session][:email].downcase) unless user
    if user && user.authenticate(params[:session][:password])
      log_in_sh(user)
      if user.class.to_s=="Patient"
        params[:session][:remember_me] == '1' ? remember_sh(user) : forget_sh(user)
      end
      redirect_back_or_sh(user)
    else
      flash.now[:danger]="Invalid email or password"
      render 'new'
    end
  end
  
  def destroy
    log_out_sh if logged_in_sh?
    redirect_to root_url
  end
  
end
