class PatientsController < ApplicationController
    # _sh: sessions_helper
    
    before_action :redirect_blocked_users_sh, except: [:show]
    before_action :redirect_blocked_admins, only: [:show]
    before_action :logged_in_patient_sh, only: [:edit, :update]
    before_action :logged_in_administrator_sh, only: [:index, :new, :create]
    before_action :correct_patient_or_admin, only: [:edit, :update, :destroy, :show]
    
    
    def show
        @patient=Patient.find(params[:id])
        @appointments=@patient.appointments.all.order(:ap_date, :ap_time)
    end
    
    def index
        @patients=Patient.paginate(page: params[:page])
      if params[:name]
        @patients = Patient.search(params[:name], 
                params[:email], 
                params[:blocked],
                params[:combine]).paginate(page: params[:page])
      else
        @patients=Patient.paginate(page: params[:page])
      end
    end
    
    def new
        @patient=Patient.new
    end
    
    def create
       @patient=Patient.new(patient_params)
       if @patient.save
           flash[:success]="Account created!"
           redirect_to @patient
       else
          render "new" 
       end
    end
    
    def edit
        @patient=Patient.find(params[:id])
    end
    
    def update
        if ( user_id = params[:user_to_block] )
        #blocking user
            unless current_user_admin_sh?
                flash[:success]="Not authorized for this action"
                redirect_to root_path and return
            end
            @patient=Patient.find(user_id)
            if @patient.blocked?
                @patient.update_attribute(:blocked, false)
                flash[:success]="Profile unblocked"
                redirect_to @patient
            else
                @patient.update_attribute(:blocked, true)
                flash[:success]="Profile blocked"
                redirect_to @patient
            end
        else
        #editing user
            @patient=Patient.find(params[:id])
            if @patient.update_attributes(patient_params)
               flash[:success]="Profile updated"
               redirect_to @patient
            else
                render 'edit'
            end
        end
    end
    
    def destroy
        user=Patient.find(params[:id])
        if current_user_sh?(user)
            flash[:success]="Your account has been deleted"
        else
            flash.now[:success]="Patient deleted"
            @id_to_delete=params[:id]
        end
        user.destroy
        respond_to do |format|
            format.html {redirect_to root_path}
            format.js {}
        end
    end
    
    private
    
    def patient_params
        params.require(:patient).permit(:name, :email, :adress, :phone, :date_birth, :password, :password_confirmation)
    end
    
    def correct_patient_or_admin
        @patient_to_edit=Patient.find(params[:id])
        redirect_to(root_url) unless( current_user_sh?(@patient_to_edit) or current_user_admin_sh? )
    end
    
    def redirect_blocked_admins
    #give blocked patients access to show while preventing it from admins
        unless current_user_sh==nil
            if current_user_sh.blocked? and current_user_admin_sh?
                redirect_to current_user_sh
            end
        end
    end
    
end
