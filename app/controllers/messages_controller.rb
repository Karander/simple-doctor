class MessagesController < ApplicationController
  # _sh: sessions_helper
  
  before_action :redirect_blocked_users_sh
  before_action :logged_in_patient_sh
  
  def new
    @message=Message.new
  end
  
  def show
    @message=Message.find(params[:id])
    if ((@message.recipient_id==current_user_sh.id or @message.recipient_id==0 ) and @message.recipient_type==current_user_sh.class.to_s)
      @message.update_attribute(:seen, true)
      remember_message_sh(@message.id)
    elsif(@message.creator_id==current_user_sh.id and @message.creator_type==current_user_sh.class.to_s)
      #do nothing
    else
      redirect_to root_path
    end
  end
  
  def create
    @message=Message.new(message_params)
    if (params[:message][:creator_id]==current_user_sh.id.to_s and params[:message][:creator_type]==current_user_sh.class.to_s)
      if @message.save
        flash[:success]="Message sent"
        redirect_to current_user_sh
      else
        render "new"
      end
    else
      redirect_to root_path
    end
  end
  
  def update
    @message=current_message_sh
    if params[:seen]=="true"
      @message.update_attribute(:seen, false)
    elsif params[:seen]=="false"
      @message.update_attribute(:seen, true)
    elsif params[:processed]=="true"
      @message.update_attribute(:processed, false)
    elsif params[:processed]=="false"
      @message.update_attribute(:processed, true)
    end
    respond_to do |format|
      format.html {redirect_to messages_path}
      format.js
    end
  end

  def index
    if current_user_admin_sh?
      @messages=Message.where(recipient_type: "Administrator", recipient_id: [0, current_user_sh.id])
      @sent_messages=Message.where(creator_type: "Administrator", creator_id: current_user_sh.id)
    else
      @messages=Message.where(recipient_type: "Patient", recipient_id: [0, current_user_sh.id])
      @sent_messages=Message.where(creator_type: "Patient", creator_id: current_user_sh.id)
    end
  end
  
  private
  
  def message_params
    if current_user_admin_sh?
      params.require(:message).permit(:creator_id, :creator_type, :content, :subject, :recipient_id, :recipient_type)
    else
      params.require(:message).permit(:creator_id, :creator_type, :content, :subject, :recipient_id)
    end
  end
  
end
