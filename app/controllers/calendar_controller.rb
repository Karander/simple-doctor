class CalendarController < ApplicationController
  # _sh: sessions_helper
  
  before_action :redirect_blocked_users_sh
  before_action :logged_in_patient_sh, only: [:show]
  
  def show
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
  end
  
end