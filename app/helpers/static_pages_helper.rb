module StaticPagesHelper
    
    def google_map_sph(center)
        "https://maps.googleapis.com/maps/api/staticmap?center=#{center}&size=520x520&zoom=17&markers=scale:2|#{center}"
    end
    
end
