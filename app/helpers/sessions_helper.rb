module SessionsHelper
    
    def log_in_sh(user)
        session[:user_id]=user.id
        session[:user_type]=user.class.to_s
    end
    
    def current_user_sh
        if (user_id = session[:user_id])
            if session[:user_type] == "Patient"
                @current_user ||= Patient.find_by(id: user_id)
            else
                @current_user ||= Administrator.find_by(id: user_id)
            end
        elsif (user_id = cookies.signed[:user_id])
            user = Patient.find_by(id: user_id)
            if user && user.authenticated?(cookies[:remember_token])
                log_in_sh(user)
                @current_user = user
            end
        end
    end
    
    def current_user_sh?(user)
        current_user_sh==user
    end
    
    def current_user_admin_sh?
        current_user_sh.class.to_s=="Administrator"
    end
    
    def logged_in_sh?
        !current_user_sh.nil?
    end
    
    def log_out_sh
        #do not use @current_user, @ causes a nil:NilClass error
        forget_sh(current_user_sh)
        session.delete(:user_id)
        session.delete(:user_type)
        @current_user=nil
    end
    
    def remember_sh(user)
        user.remember
        cookies.permanent.signed[:user_id]=user.id
        cookies.permanent[:remember_token]=user.remember_token
    end
    
    def forget_sh(user)
        user.forget
        cookies.delete(:user_id)
        cookies.delete(:remember_token)
    end
    
    def store_location
        session[:forwarding_url]=request.original_url if request.get?
    end
    
    def redirect_back_or_sh(default)
        redirect_to(session[:forwarding_url] || default)
        session.delete(:forwarding_url)
    end
    
    def logged_in_patient_sh
        unless logged_in_sh?
            store_location
            flash[:danger]="Please log in"
            redirect_to login_path
        end
    end
    
    def logged_in_administrator_sh
        unless current_user_admin_sh?
            flash[:danger]="You are not authorized for this action"
            redirect_to root_path
        end
    end
    
    def redirect_blocked_users_sh
        unless current_user_sh==nil
            redirect_to current_user_sh if current_user_sh.blocked?
        end
    end
    
    def remember_message_sh(id)
       session[:message_id]=id 
    end
    
    def current_message_sh
       if(message_id=session[:message_id]) 
           @msg=Message.find_by(id: message_id)
       end
    end
    
    def edit_session_sh?
        if(value=session[:edit])
            value
        end
    end
    
    def switch_edit_sh
        if session[:edit]
            session.delete(:edit)
        else
            if (current_user_admin_sh? and current_user_sh.supervisor)
                session[:edit]=true
            end
        end
    end
    
end
