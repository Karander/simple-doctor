module CalendarHelper
  
  def calendar(date = Date.today, &block)
    Calendar.new(self, date, block, current_doctor_ch).table
  end
  
  def current_doctor_ch
    if params[:doctor].nil?
      @doctor ||= Doctor.first
    else
      @doctor = Doctor.find_by(id: params[:doctor])
    end
  end
  
end