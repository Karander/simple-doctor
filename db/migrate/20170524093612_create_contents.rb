class CreateContents < ActiveRecord::Migration[5.0]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :content
      t.string :object
      t.attachment :image

      t.timestamps
    end
  end
end
