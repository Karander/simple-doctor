class CreateAdministrators < ActiveRecord::Migration[5.0]
  def change
    create_table :administrators do |t|
      t.boolean :supervisor, default: false
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
