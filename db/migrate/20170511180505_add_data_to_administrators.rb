class AddDataToAdministrators < ActiveRecord::Migration[5.0]
  def change
    add_column :administrators, :phone, :string
    add_column :administrators, :adress, :string
    add_column :administrators, :date_birth, :date
  end
end
