class AddBlockedToPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :patients, :blocked, :boolean, default: false
  end
end
