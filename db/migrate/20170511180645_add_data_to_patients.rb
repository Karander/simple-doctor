class AddDataToPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :patients, :adress, :string
    add_column :patients, :date_birth, :date
  end
end
