class AddRememberDigestToPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :patients, :remember_digest, :string
  end
end
