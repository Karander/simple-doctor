class AddPhoneToPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :patients, :phone, :string
  end
end
