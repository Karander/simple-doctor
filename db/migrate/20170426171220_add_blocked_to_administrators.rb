class AddBlockedToAdministrators < ActiveRecord::Migration[5.0]
  def change
    add_column :administrators, :blocked, :boolean, default: false
  end
end
