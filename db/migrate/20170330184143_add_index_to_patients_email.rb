class AddIndexToPatientsEmail < ActiveRecord::Migration[5.0]
  def change
    add_index :patients, :email, unique: true
  end
end
