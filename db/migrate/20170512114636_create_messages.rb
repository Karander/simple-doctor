class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :content
      t.text :subject
      t.integer :creator_id
      t.text :creator_type
      t.integer :recipient_id, default: 0
      t.text :recipient_type, default: "Administrator"
      t.boolean :seen, default: false
      t.boolean :processed, default: false

      t.timestamps
    end
  end
end
