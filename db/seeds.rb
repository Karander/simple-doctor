Patient.create!(name: "Max Mustermann", email: "mustermail@hotmail.com", password: "password", password_confirmation: "password")

50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  Patient.create!(name:  name,
               email: email,
               date_birth: "1982-11-17",
               password:              password,
               password_confirmation: password)
end

5.times do |n|
  name  = Faker::Name.name
  Doctor.create!(name:  name)
end

3.times do |n|
  name  = Faker::Name.name
  email = "administrator-#{n+1}@railstutorial.org"
  password = "password"
  Administrator.create!(name:  name,
               email: email,
               date_birth: "1982-11-17",
               password:              password,
               password_confirmation: password)
end

Administrator.create!(name:  "Kim Yum",
             email: "kimyummail@mail.com",
             date_birth: "1982-11-17",
             supervisor: true,
             password:              "password",
             password_confirmation: "password")
             
Administrator.create!(name:  "Tim Yum",
             email: "timyummail@mail.com",
             date_birth: "1982-11-17",
             supervisor: true,
             password:              "password",
             password_confirmation: "password")
             
Content.new(title: "home",
                content: "stuff tvjkbjfövjböf jgdöfgjdöfgjdöfgjldö fdgjkdfögjö dflkgjdlgj lkdsjljwejwö sewjsdlfjs
                dfgsldfsödkfj djkfsdf jslldk klsdjflskfjselj  ql3kjasljflsh lkhdflvna lskadfj lkdj lsfkj slkdjsl kja a dklfjadflj
                jsjkgslö kjdl asdj aledjwkl  akeflkfj  sdlfkjsl   lkfjslfjew dlfkdjölgdlögj
                sdkfjsdöfjs ödfls dklf eröljewö wö ölsdföl öwöwsöldjöj4ölj 
                dlfjslkdfjlsdfj kjwöerjwö jlöklweörkwök3 rökwököwkfö ödjqhhialkfase4öqpvi udsöjfg 
                skldjflskdjf lsuwplfjasdökfjweritdergkt5izdogdh lsdfjsldfh").save(validate: false)
Content.new(title: "about",
                content: "stuff tvjkbjfövjböf jgdöfgjdöfgjdöfgjldö fdgjkdfögjö dflkgjdlgj lkdsjljwejwö sewjsdlfjs
                dlfjslkdfjlsdfj kjwöerjwö jlöklweörkwök3 rökwököwkfö ödjqhhialkfase4öqpvi udsöjfg 
                skldjflskdjf lsuwplfjasdökfjweritdergkt5izdogdh lsdfjsldfh").save(validate: false)
