# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170524093612) do

  create_table "administrators", force: :cascade do |t|
    t.boolean  "supervisor",      default: false
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "blocked",         default: false
    t.string   "phone"
    t.string   "adress"
    t.date     "date_birth"
  end

  create_table "appointments", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.date     "ap_date"
    t.string   "ap_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_appointments_on_doctor_id"
    t.index ["patient_id"], name: "index_appointments_on_patient_id"
  end

  create_table "contents", force: :cascade do |t|
    t.string   "title"
    t.string   "content"
    t.string   "object"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "content"
    t.text     "subject"
    t.integer  "creator_id"
    t.text     "creator_type"
    t.integer  "recipient_id",   default: 0
    t.text     "recipient_type", default: "Administrator"
    t.boolean  "seen",           default: false
    t.boolean  "processed",      default: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "phone"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "blocked",         default: false
    t.string   "adress"
    t.date     "date_birth"
    t.index ["email"], name: "index_patients_on_email", unique: true
  end

end
