Rails.application.routes.draw do

  root 'static_pages#home'
  
  get '/contact', to: 'static_pages#contact'
  get '/about', to: 'static_pages#about'
  get '/signup_patient', to: 'patients#new'
  
  get '/login', to: "sessions#new"
  post '/login', to: "sessions#create"
  patch '/edit_session', to: "sessions#update"
  delete "/logout", to: "sessions#destroy"
  
  resources :patients
  resources :contents
  resources :messages
  resources :appointments
  resources :administrators
  resource :calendar, only: [:show], controller: :calendar
  
end
