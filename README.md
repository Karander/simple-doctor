# Needs to be fixed
- CalendarControllerTest  
testing not complete; unexpected error on get calendar_path even though aplication is functional
- CalendarShow  
dropdowns in the header do not work while in Calandar/Show
- PatientsShow  
list of appointments ignores the scss rule body{padding-bottom: 45px} in common.scss. Tempraly contained by
reducing the max-height of the list in patients.scss.  


# Works but should be improved
- !!!MAJOR!!! editor mode  
    10~20% of the times, turning editor mode on or off, will cause server error "connection lost". Clicking on "Try again"
    fixes it.
- appointments/destroy.js.erb  
    crappy solution for the flash message
- date_birth validation for patients and administrators   
    "uninformative" error msgs# Simple docotr README
- all new.html.erb and edit.html.erb   
    in common.scss -> /* forms */ -> input {margin-bottom: 15px} seems not to work for the submit buttons. 
    Fixed by adding id: "fix" to those and #fix{margin-top: 15px} in common.scss. 