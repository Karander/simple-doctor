require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  
  def setup
    @non_sunday=Date.tomorrow
    @sunday=Date.tomorrow
    @saturday=Date.tomorrow
    @today=Date.today
    @past=Date.yesterday
    (@non_sunday += 1) if @non_sunday.sunday?
    (@sunday += 1) until @sunday.sunday?
    (@saturday += 1) until @saturday.saturday?
    @patient=patients(:michael)
    @other_patient=patients(:archer)
    @doctor=doctors(:doc_brown)
    @other_doctor=doctors(:doc_blue)
    @appointment=Appointment.new(ap_date: @non_sunday, ap_time: "17:00", doctor_id: @doctor.id, patient_id: @patient.id)
  end
  
  test "dependent on patients" do
    assert_difference "Appointment.count", 1 do
      @appointment.save
    end
    assert_difference "Appointment.count", -2 do
    #-2 because of the one fixture for michael
      @patient.destroy
    end
  end
  
  test "should be valid" do
    assert @appointment.valid?
  end
  
  test "non of the values can be nil" do
    @appointment.ap_date=nil
    assert_not @appointment.valid?
    @appointment.ap_date=@non_sunday
    @appointment.ap_time=nil
    assert_not @appointment.valid?
    @appointment.ap_time="8:00"
    @appointment.doctor_id=nil
    assert_not @appointment.valid?
    @appointment.doctor_id=@doctor.id
    @appointment.patient_id=nil
    assert_not @appointment.valid?
  end
  
  test "no appointments on Sunday, today or in the past" do
    @appointment.ap_date=@sunday
    assert_not @appointment.valid?
    @appointment.ap_date=@today
    assert_not @appointment.valid?
    @appointment.ap_date=@past
    assert_not @appointment.valid?
  end
  
  test "limited_workinghours_on_saturday" do
    @appointment.ap_date=@saturday
    assert @appointment.valid?
    @appointment.ap_time="20:00"
    assert_not @appointment.valid?
  end
    
  
  test "ap_time unique for doctor-date and patient-date" do
    #ceate an appointment with same data to assert that the same entry can not be given twice
    appointment_1=Appointment.create(ap_date: @non_sunday, ap_time: "17:00", 
                      doctor_id: @doctor.id, patient_id: @patient.id)
    assert_not @appointment.valid?
    #assert that the same entry can not exist for different doctors
    @appointment.doctor_id=@other_doctor.id
    assert_not @appointment.valid?
    @appointment.doctor_id=@doctor.id #get the old value
    #assert that the same entry can not exist for different patients
    appointment_2=Appointment.new(ap_date: @non_sunday, ap_time: "17:00",
                      doctor_id: @doctor.id, patient_id: @other_patient.id)
    assert_not appointment_2.valid?
    #assert that the same entry can exist for different days 
    @appointment.ap_date += 1 #incase today is already monday
    (@appointment.ap_date += 1) until @appointment.ap_date.monday?
    assert @appointment.valid?
  end
  
end
