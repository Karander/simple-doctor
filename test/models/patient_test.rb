require 'test_helper'

class PatientTest < ActiveSupport::TestCase
  
  def setup
    @patient=Patient.new(name: "Test Patient One", email: "examplemail@mail.com",
                            password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @patient.valid?
  end
  
  test "should require valid name" do
    @patient.name=" "
    assert_not @patient.valid?
    @patient.name="aaaa"
    assert_not @patient.valid?
    @patient.name="a"*51
    assert_not @patient.valid?
  end
  
  test "should require valid email" do
    @patient.email=" "
    assert_not @patient.valid?
    @patient.email="a"*256
    assert_not @patient.valid?
    valid_adress = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    invalid_adress = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    valid_adress.each do |adress|
      @patient.email=adress
      assert @patient.valid?, "#{adress.inspect} should be valid"
    end
    invalid_adress.each do |adress|
      @patient.email=adress
      assert_not @patient.valid?, "#{adress.inspect} should be invalid"
    end
  end
  
  test "email should be unique" do
    dupl_pat=@patient.dup
    @patient.save
    assert_not dupl_pat.valid?
    dupl_pat.email.upcase!
    assert_not dupl_pat.valid?
  end
  
  test "password should be valid" do
    @patient.password = @patient.password_confirmation = " " * 6
    assert_not @patient.valid?
    @patient.password = @patient.password_confirmation = "a" * 5
    assert_not @patient.valid?
  end

  
end
