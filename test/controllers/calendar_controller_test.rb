require 'test_helper'

class CalendarControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
  end
  
  test "should get show only for logged in patients" do
    get calendar_path
    assert_redirected_to login_path
  end
  
  #testing not complete; unexpected error on get calendar_path even though aplication is functional

end
