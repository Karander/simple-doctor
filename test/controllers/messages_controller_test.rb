require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @administrator=administrators(:basic_admin)
  end
  
  test "all actions only if logged in" do
    get new_message_path
    assert_redirected_to login_path
    get messages_path
    assert_redirected_to login_path
    log_in_as(@patient)
    get new_message_path
    assert_template "messages/new"
    get messages_path
    assert_template "messages/index"
    delete logout_path
    get messages_path
    assert_redirected_to login_path
    log_in_as(@administrator)
    get new_message_path
    assert_template "messages/new"
    get messages_path
    assert_template "messages/index"
  end
    

end
