require 'test_helper'

class PatientsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @other_patient=patients(:archer)
    @administrator=administrators(:basic_admin)
  end
  
  test "index only for logged in adminitrators" do
    get patients_path
    assert_redirected_to root_path
  end
  
  test "show only for administrator or self" do
    get patient_path(@patient)
    assert_redirected_to root_path
    log_in_as(@other_patient)
    get patient_path(@patient)
    assert_redirected_to root_path
    log_in_as(@administrator)
    get patient_path(@patient)
    assert_template "patients/show"
    assert_match "block", response.body
    log_in_as(@patient)
    get patient_path(@patient)
    assert_template "patients/show"
  end
    
  test "should not allow delete if wrong patient" do
    assert_no_difference "Patient.count" do
      delete patient_path(@patient)
    end
    assert_redirected_to root_path
    log_in_as(@other_patient)
    assert_no_difference "Patient.count" do
      delete patient_path(@patient)
    end
    assert_redirected_to root_path
  end
  
  test "self should delete patient" do
    log_in_as(@other_patient)
    assert_difference "Patient.count", -1 do
      delete patient_path(@other_patient)
    end
    assert_redirected_to root_path
  end
  
  test "admin should delete patient" do
    log_in_as(@administrator)
    assert_difference "Patient.count", -1 do
      delete patient_path(@other_patient)
    end
    assert_redirected_to root_path
  end
  
end
