require 'test_helper'

class AdministratorsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
   @administrator=administrators(:basic_admin)
   @supervisor=administrators(:super_admin)
  end
 
  test "should get new only for logged in supervisor" do
    get new_administrator_path
    assert_redirected_to root_path
    log_in_as @administrator
    get new_administrator_path
    assert_redirected_to root_path
    log_in_as @supervisor
    get new_administrator_path
    assert_response :success
  end

  test "should get show only for logged in admin" do
    get administrator_path(@administrator)
    assert_redirected_to root_path
    log_in_as @administrator
    get administrator_path(@administrator)
    assert_response :success
  end

  test "should get index only for logged in admin" do
    get administrators_path
    assert_redirected_to root_path
    log_in_as @administrator
    get administrators_path
    assert_response :success
  end

end
