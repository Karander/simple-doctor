require 'test_helper'

class AppointmentsControllerTest < ActionDispatch::IntegrationTest
    
    def setup
        @patient=patients(:michael)
        @other_patient=patients(:archer)
        @appointment=appointments(:test_appointment)
        @administrator=administrators(:basic_admin)
    end
    
    test "unauthorized user can not delete appointments" do
        assert_no_difference "Appointment.count" do
            delete appointment_path(@appointment)
        end
        assert_redirected_to login_path
        log_in_as(@other_patient)
        assert_no_difference "Appointment.count" do
            delete appointment_path(@appointment)
        end
        assert_redirected_to root_path
    end
    
    test "authorized user can delete appointments" do
        log_in_as(@patient)
        assert_difference "Appointment.count", -1 do
            delete appointment_path(@appointment)
        end
        assert_redirected_to @patient
        follow_redirect!
        assert_match "Appointment deleted", response.body
    end
    
    test "administrator can delete appointments" do
        log_in_as(@administrator)
        assert_difference "Appointment.count", -1 do
            delete appointment_path(@appointment)
        end
        assert_redirected_to @patient
    end
    
    test "index only for administrators" do
        get appointments_path
        assert_redirected_to root_path
        log_in_as(@patient)
        get appointments_path
        assert_redirected_to root_path
        log_in_as(@administrator)
        get appointments_path
        assert_template "appointments/index"
    end

end
