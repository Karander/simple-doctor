require 'test_helper'

class MessageCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @other_patient=patients(:archer)
    @administrator=administrators(:basic_admin)
  end
  
  test "successful create" do
    assert_no_difference "Message.count" do
      post messages_path, params: { message: { creator_id: @patient.id, creator_type: @patient.class.to_s,
                                          subject: "1", content: "2"} }
      assert_redirected_to login_path
    end
    log_in_as(@patient)
    assert_difference "Message.count", 1 do
      post messages_path, params: { message: { creator_id: @patient.id, creator_type: @patient.class.to_s,
                                          subject: "1", content: "2"} }
      assert_redirected_to @patient
      assert_not flash.empty?
    end
    delete logout_path
    log_in_as(@administrator)
    assert_difference "Message.count", 1 do
      post messages_path, params: { message: { creator_id: @administrator.id, creator_type: @administrator.class.to_s,
                                          subject: "1", content: "2"} }
      assert_redirected_to @administrator
      assert_not flash.empty?
    end
  end
  
  test "prevent create with false creator-id/type" do
    log_in_as(@patient)
    assert_no_difference "Message.count" do
      post messages_path, params: { message: { creator_id: @patient.id+1, creator_type: @patient.class.to_s,
                                          subject: "1", content: "2"} }
      assert_redirected_to root_path
    end
    assert_no_difference "Message.count" do
      post messages_path, params: { message: { creator_id: @patient.id+1, creator_type: @administrator.class.to_s,
                                          subject: "1", content: "2"} }
      assert_redirected_to root_path
    end
  end
  
  test "subject and content present" do
    log_in_as(@patient)
    assert_no_difference "Message.count" do
      post messages_path, params: { message: { creator_id: @patient.id, creator_type: @patient.class.to_s,
                                          subject: "", content: "2"} }
    end
    assert_template "messages/new"
    assert_match "blank", response.body
    assert_no_difference "Message.count" do
      post messages_path, params: { message: { creator_id: @patient.id, creator_type: @patient.class.to_s,
                                          subject: "1", content: ""} }
    end
    assert_template "messages/new"
    assert_match "blank", response.body
  end
  
end
