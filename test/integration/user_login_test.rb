require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
  end
  
  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: { session: { email: " ", password: "ddg"} }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "patient login and logout with valid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: { session: { email: "michael@example.com", password: "password"} }
    assert_redirected_to @patient
    follow_redirect!
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", patient_path(@patient)
    assert_select "a[href=?]", patient_path(@patient.id, :user_to_block => @patient.id), count: 0
    assert is_logged_in?
    delete logout_path
    assert_redirected_to root_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", patient_path(@patient), count: 0
    assert_not is_logged_in?
    delete logout_path
  end
  
  test "authenticated? should return false id remember_digest is nil" do
    assert_not @patient.authenticated?(' ')
  end
  
  test "login with remember" do
    #inside test symbols (":abc") for cookies return always nil; use string
    log_in_as(@patient)
    assert_not_empty cookies['remember_token']
  end
  
  test "login without remember" do
    #first log in with remember to set the token, then again without to check if it got deleted
    #inside test symbols (":abc") for cookies return always nil; use string
    log_in_as(@patient)
    log_in_as(@patient, remember_me: "0")
    assert_empty cookies['remember_token']
  end
  
end
