require 'test_helper'

class BlockingUsersTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @administrator=administrators(:basic_admin)
    @supervisor=administrators(:super_admin)
  end
  
  test "patients can not block" do
    log_in_as(@patient)
    patch patient_path(@patient), params: { user_to_block: @patient.id }
    assert_redirected_to root_path
    assert_not flash.empty?
    @patient.reload
    assert_not @patient.blocked?
    patch administrator_path(@administrator), params: { user_to_block: @administrator.id }
    assert_redirected_to root_path
    assert_not flash.empty?
    @administrator.reload
    assert_not @administrator.blocked?
  end
  
  test "non supervisor can not block administrators" do
    log_in_as(@administrator)
    patch administrator_path(@administrator), params: { user_to_block: @administrator.id }
    assert_redirected_to root_path
    assert_not flash.empty?
    @administrator.reload
    assert_not @administrator.blocked?
  end
  
  test "supervisor can not be blocked" do
    log_in_as(@supervisor)
    patch administrator_path(@supervisor), params: { user_to_block: @supervisor.id }
    assert_redirected_to administrator_path(@supervisor)
    assert_not flash.empty?
    @supervisor.reload
    assert_not @supervisor.blocked?
  end
  
  test "successful patient block and unblock" do
    log_in_as(@administrator)
    assert_not @patient.blocked?
    patch patient_path(@patient), params: { user_to_block: @patient.id }
    assert_redirected_to patient_path(@patient)
    assert_not flash.empty?
    @patient.reload
    assert @patient.blocked?
    patch patient_path(@patient), params: { user_to_block: @patient.id }
    assert_redirected_to patient_path(@patient)
    assert_not flash.empty?
    @patient.reload
    assert_not @patient.blocked?
  end
  
  test "successful administrator block and unblock" do
    log_in_as(@supervisor)
    assert_not @administrator.blocked?
    patch administrator_path(@administrator), params: { user_to_block: @administrator.id }
    assert_redirected_to administrator_path(@administrator)
    assert_not flash.empty?
    @administrator.reload
    assert @administrator.blocked?
    patch administrator_path(@administrator), params: { user_to_block: @administrator.id }
    assert_redirected_to administrator_path(@administrator)
    assert_not flash.empty?
    @administrator.reload
    assert_not @administrator.blocked?
  end
  
  test "restricted access for blocked patients" do
    @patient.update_attribute("blocked", true)
    @patient.reload
    assert @patient.blocked?
    log_in_as(@patient)
    follow_redirect!
    assert_match "this account is blocked", response.body
    get calendar_path
    assert_redirected_to patient_path(@patient)
    get edit_patient_path(@patient)
    assert_redirected_to patient_path(@patient)
    get new_appointment_path
    assert_redirected_to patient_path(@patient)
    get new_message_path
    assert_redirected_to patient_path(@patient)
  end
  
  test "restricted access for blocked administrators" do
    @administrator.update_attribute("blocked", true)
    @administrator.reload
    assert @administrator.blocked?
    log_in_as(@administrator)
    follow_redirect!
    assert_match "this account is blocked", response.body
    get calendar_path
    assert_redirected_to administrator_path(@administrator)
    get edit_patient_path(@patient)
    assert_redirected_to administrator_path(@administrator)
    get new_appointment_path
    assert_redirected_to administrator_path(@administrator)
    get patient_path(@patient)
    assert_redirected_to administrator_path(@administrator)
    get new_message_path
    assert_redirected_to administrator_path(@administrator)
  end
  
end
