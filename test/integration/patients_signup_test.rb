require 'test_helper'

class PatientsSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    @administrator=administrators(:basic_admin)
    @patient=patients(:archer)
  end
  
  test "invalid signup" do
    log_in_as(@administrator)
    get signup_patient_path
    assert_no_difference "Patient.count" do
      post patients_path, params: { patient: { name: " ", email: "fdfdf", 
                                                password: "a", password_confirmation: "c" } }
      end
    assert_template "patients/new"
  end
  
  test "valid signup" do
    log_in_as(@administrator)
    get signup_patient_path
    assert_difference "Patient.count", 1 do
      post patients_path, params: { patient: { name: "Max Mustermann", email: "mustermail@htmail.com", 
                                                password: "password", password_confirmation: "password" } }
      end
    follow_redirect!
    assert_template "patients/show"
  end
  
  test "valid signup only for administrators" do
    get signup_patient_path
    assert_redirected_to root_path
    log_in_as(@patient)
    get signup_patient_path
    assert_redirected_to root_path
    follow_redirect!
    assert_no_difference "Patient.count" do
      post patients_path, params: { patient: { name: "Max Mustermann", email: "mustermail@htmail.com", 
                                                password: "password", password_confirmation: "password" } }
      end
    assert_redirected_to root_path
  end
  
end
