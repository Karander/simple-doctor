require 'test_helper'

class AppointmentsCreationTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
  end
  
  test "valid appointment for logged in user" do
    get new_appointment_path, params: { doctor_id: 1337, ap_date: Date.today.next_weekday }
    assert_redirected_to login_path
    assert_no_difference "Appointment.count" do
      post appointments_path, params: { appointment: { ap_time: "11:00", ap_date: Date.today.next_weekday,
                                            doctor_id: 1337, patient_id: @patient.id} }
    end
    assert_redirected_to login_path
    log_in_as(@patient)
    get new_appointment_path, params: { doctor_id: 1337, ap_date: Date.today.next_weekday }
    assert_difference "Appointment.count", 1 do
      post appointments_path, params: { appointment: { ap_time: "11:00", ap_date: Date.today.next_weekday,
                                            doctor_id: 1337, patient_id: @patient.id} }
    end
    assert_redirected_to @patient
  end
  
  test "invalid appointment creation for patient" do
    log_in_as(@patient)
    assert_no_difference "Appointment.count" do
      post appointments_path, params: { appointment: { ap_time: nil, ap_date: Date.today.next_weekday,
                                            doctor_id: 1337, patient_id: @patient.id} }
    end
    assert_redirected_to calendar_path
    assert_no_difference "Appointment.count" do
      post appointments_path, params: { appointment: { ap_time: "11:00", ap_date: nil,
                                            doctor_id: 1337, patient_id: @patient.id} }
    end
    assert_no_difference "Appointment.count" do
      post appointments_path, params: { appointment: { ap_time: "11:00", ap_date: Date.today.next_weekday,
                                            doctor_id: nil, patient_id: @patient.id} }
    end
  end
  
end
