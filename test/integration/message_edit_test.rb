require 'test_helper'

class MessageEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @administrator=administrators(:basic_admin)
    @patient=patients(:michael)
    @msg=messages(:msg_AtoP)
    @msg_2=messages(:msg_PtoA)
  end
  
  test "seen and unseen" do
    log_in_as(@patient)
    get messages_path
    assert_template "messages/index"
    assert_match "<b>test 1337</b>", response.body
    get message_path(@msg)
    assert_match "content 1337", response.body
    assert_match "Set to: Unseen", response.body
    patch message_path(@msg), params: { seen: "true" }
    follow_redirect!
    assert_match "<b>test 1337</b>", response.body
    get message_path(@msg)
    get messages_path
    assert_match "<p>test 1337</p>", response.body
  end
  
  test "processed" do
    log_in_as(@administrator)
    get message_path(@msg_2)
    assert_match "Set to: Completed", response.body
    patch message_path(@msg_2), params: { processed: "false" }
    get message_path(@msg_2)
    assert_match "Set to: In process", response.body
    patch message_path(@msg_2), params: { processed: "true" }
    get message_path(@msg_2)
    assert_match "Set to: Completed", response.body
  end
  
end
