require 'test_helper'

class AdministratorLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @administrator=administrators(:basic_admin)
    @administrator_2=administrators(:basic_admin_2)
    @supervisor=administrators(:super_admin)
  end
  
  test "valid login with logout" do
    post login_path, params: { session: { email: @administrator.email, password: "password"} }
    assert flash.empty?
    assert_redirected_to @administrator
    follow_redirect!
    assert_select "a[href=?]", patients_path, count: 1
    assert_select "a[href=?][data-method=delete]", administrator_path, count: 0
    assert_select "a[href=?][data-method=patch]", administrator_path(@administrator.id, :user_to_block => @administrator.id), count: 0
    assert_select "a[href=?]", edit_administrator_path(@administrator), count: 0
    assert_template "administrators/show"
    delete logout_path
    assert_redirected_to root_path
    follow_redirect!
    assert_not is_logged_in?
    assert_select "a[href=?]", patients_path, count: 0
    get administrator_path(@administrator.id)
    assert_redirected_to root_path
    follow_redirect!
    assert_match "not authorized", response.body
    post login_path, params: { session: { email: @supervisor.email, password: "password"} }
    follow_redirect!
    assert_select "a[href=?]", patients_path, count: 1
    assert_select "a[href=?][data-method=delete]", administrator_path, count: 0
    assert_select "a[href=?][data-method=patch]", administrator_path(@supervisor.id, :user_to_block => @supervisor.id), count: 0
    assert_select "a[href=?]", edit_administrator_path(@supervisor), count: 1
    get administrator_path(@administrator.id)
    assert_select "a[href=?][data-method=delete]", administrator_path, count: 1
    assert_select "a[href=?][data-method=patch]", administrator_path(@administrator.id, :user_to_block => @administrator.id), count: 1
    assert_select "a[href=?]", edit_administrator_path(@supervisor), count: 1
  end
  
  test "correct_administrator_or_supervisor" do
    log_in_as(@administrator)
    get administrator_path(@administrator_2.id)
    assert_redirected_to root_path
    follow_redirect!
    assert_match "not authorized", response.body
    delete logout_path
    log_in_as(@supervisor)
    get administrator_path(@administrator_2.id)
    assert_template "administrators/show"
  end
  
end
