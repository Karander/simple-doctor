require 'test_helper'

class PatientsIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient = patients(:michael)
    @administrator = administrators(:basic_admin)
  end

  test "index including pagination only for administrators" do
    log_in_as(@patient)
    get patients_path
    assert_redirected_to root_path
    follow_redirect!
    assert_match "not authorized", response.body
    log_in_as(@administrator)
    get patients_path
    assert_template 'patients/index'
    assert_select 'div.pagination'
    Patient.paginate(page: 1).each do |patient|
      assert_select 'a[href=?]', patient_path(patient), text: patient.name
    end
  end
  
end
