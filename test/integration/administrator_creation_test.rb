require 'test_helper'

class AdministratorCreationTest < ActionDispatch::IntegrationTest
  
  def setup
    @supervisor=administrators(:super_admin)
    @administrator=administrators(:basic_admin)
  end
  
  test "create a new valid administrator" do
    log_in_as(@supervisor)
    get new_administrator_path
    assert_template "administrators/new"
    assert_difference "Administrator.count", 1 do
      post administrators_path, params: { administrator: { name: "test admin", email: "testadminmail@mail.com", date_birth: "1955-12-11",
                                            password: "password", password_confirmation: "password"} }
    end
    assert_redirected_to @supervisor
  end
  
  test "create a new valid administrator only for supervisors" do
    log_in_as(@administrator)
    assert_no_difference "Administrator.count" do
      post administrators_path, params: { administrator: { name: "test admin", email: "testadminmail@mail.com", date_birth: "1955-12-11",
                                            password: "password", password_confirmation: "password"} }
    end
  end
  
  test "invalid administrator creation" do
    log_in_as(@supervisor)
    assert_no_difference "Administrator.count" do
      post administrators_path, params: { administrator: { name: "test admin", email: "", date_birth: "1955-12-11",
                                            password: "password", password_confirmation: "password"} }
    end
    assert_no_difference "Administrator.count" do
      post administrators_path, params: { administrator: { name: "", email: "testadminmail@mail.com", date_birth: "1955-12-11",
                                            password: "password", password_confirmation: "password"} }
    end
    assert_no_difference "Administrator.count" do
      post administrators_path, params: { administrator: { name: "test admin", email: "testadminmail@mail.com", date_birth: "1955-12-11",
                                            password: "", password_confirmation: ""} }
    end
  end
  
  test "can not create a supervisor" do
    log_in_as(@supervisor)
    post administrators_path, params: { administrator: { name: "test admin", email: "testadminmail@mail.com", date_birth: "1955-12-11",
                                            password: "password", password_confirmation: "password", supervisor: "true"} }
    new_admin=Administrator.find_by(email: "testadminmail@mail.com")
    assert_not new_admin.supervisor==true
  end
  
  test "deleting administrators for supervisors" do
    log_in_as(@supervisor)
    follow_redirect!
    assert_select "a[href=?][data-method=delete]", administrator_path, count: 0
    get administrator_path(@administrator)
    assert_select "a[href=?][data-method=delete]", administrator_path, count: 1
    assert_difference "Administrator.count", -1 do
      delete administrator_path(@administrator)
    end
    assert_not flash.empty?
    assert_redirected_to @supervisor
  end
  
  test "supervisors can not be deleted" do
    log_in_as(@supervisor)
    assert_no_difference "Administrator.count" do
      delete administrator_path(@supervisor)
    end
    assert_not flash.empty?
    assert_redirected_to @supervisor
  end
  
  test "deleting administrators only for supervisors" do
    log_in_as(@administrator)
    assert_no_difference "Administrator.count" do
      delete administrator_path(@administrator)
    end
    assert_not flash.empty?
    assert_redirected_to root_path
  end
  
end
