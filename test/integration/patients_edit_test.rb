require 'test_helper'

class PatientsEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @other_patient=patients(:archer)
    @administrator=administrators(:basic_admin)
  end
  
  test "before_action loggen_in_patient" do
    get edit_patient_path(@patient)
    assert_redirected_to login_path
    assert_not flash.empty?
    follow_redirect!
    assert_template "sessions/new"
    patch patient_path(@patient), params: { patient: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_redirected_to login_path
  end
  
  test "should redirect wrong patients" do
    log_in_as(@other_patient)
    get edit_patient_path(@patient)
    assert_redirected_to root_url
    assert flash.empty?
    follow_redirect!
    patch patient_path(@patient), params: { patient: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_redirected_to root_url
    assert flash.empty?
  end
    
  
  test "unsuccessful edit" do
    log_in_as(@patient)
    get edit_patient_path(@patient)
    assert_template "patients/edit"
    patch patient_path(@patient), params: { patient: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_template "patients/edit"
    assert_match "Password is too short", body
  end
  
  test "successful self edit with friendly forwarding" do
    get edit_patient_path(@patient)
    log_in_as(@patient)
    assert_redirected_to edit_patient_path(@patient)
    follow_redirect!
    assert_template "patients/edit"
    name="Max Powers"
    patch patient_path(@patient), params: { patient: { name:  name,
                                              email: @patient.email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_redirected_to @patient
    assert_not flash.empty?
    @patient.reload
    assert_equal name, @patient.name
  end
  
  test "successful admin edit with friendly forwarding" do
    get edit_patient_path(@patient)
    log_in_as(@administrator)
    assert_redirected_to edit_patient_path(@patient)
    follow_redirect!
    assert_template "patients/edit"
    name="Max Powers"
    patch patient_path(@patient), params: { patient: { name:  name,
                                              email: @patient.email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_redirected_to @patient
    assert_not flash.empty?
    @patient.reload
    assert_equal name, @patient.name
  end
  
end
