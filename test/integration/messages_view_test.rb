require 'test_helper'

class MessagesViewTest < ActionDispatch::IntegrationTest
  
  def setup
    @patient=patients(:michael)
    @other_patient=patients(:archer)
    @msg=messages(:msg_PtoA)
  end
  
  test "available only to correct user" do
    log_in_as(@patient)
    get message_path(@msg)
    assert_template "messages/show"
    delete logout_path
    log_in_as(@other_patient)
    get message_path(@msg)
    assert_redirected_to root_path
  end
  
end
