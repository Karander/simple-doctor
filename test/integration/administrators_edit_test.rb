require 'test_helper'

class AdministratorsEditTest < ActionDispatch::IntegrationTest

  def setup
    @patient=patients(:michael)
    @supervisor=administrators(:super_admin)
    @administrator=administrators(:basic_admin)
  end
  
  test "before_action loggen_in_administrator" do
    get edit_administrator_path(@administrator)
    assert_redirected_to root_path
    assert_not flash.empty?
    patch administrator_path(@administrator), params: { administrator: { name:  "",
                                              email: "foo@invalid",
                                              date_birth: "1988-04-12",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_redirected_to root_path
    log_in_as(@patient)
    get edit_administrator_path(@administrator)
    assert_redirected_to root_path
    assert_not flash.empty?
    patch administrator_path(@administrator), params: { administrator: { name:  "",
                                              email: "foo@invalid",
                                              date_birth: "1988-04-12",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_redirected_to root_path
  end
  
  test "edit only for supervisors" do
    log_in_as(@administrator)
    get edit_administrator_path(@administrator)
    assert_redirected_to root_path
    assert_not flash.empty?
    patch administrator_path(@administrator), params: { administrator: { name:  "",
                                              email: "foo@invalid",
                                              date_birth: "1988-04-12",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_redirected_to root_url
  end
    
  
  test "unsuccessful edit" do
    log_in_as(@supervisor)
    get edit_administrator_path(@administrator)
    assert_template "administrators/edit"
    patch administrator_path(@administrator), params: { administrator: { name:  "",
                                              email: "foo@invalid",
                                              date_birth: "1988-04-12",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_template "administrators/edit"
    assert_match "be blank", response.body
  end
  
  test "successful edit" do
    log_in_as(@supervisor)
    get edit_administrator_path(@administrator)
    name="Max Powers"
    patch administrator_path(@administrator), params: { administrator: { name:  name,
                                              email: @administrator.email,
                                              date_birth: "1988-04-12",
                                              password:              "",
                                              password_confirmation: "" } }
    assert_redirected_to @administrator
    assert_not flash.empty?
    @administrator.reload
    assert_equal name, @administrator.name
  end
  
end
