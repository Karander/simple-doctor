require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

# _sh: sessions_helper

    def setup
        @patient=patients(:michael)
        remember_sh(@patient)
    end

    test "current_user returns right user when session is nil" do
        assert_equal @patient, current_user_sh
        assert is_logged_in?
    end
    
    test "current_user returns nil when remember digest is wrong" do
        @patient.update_attribute(:remember_digest, Patient.digest(Patient.new_token))
        assert_nil current_user_sh
    end
    
end